# Changelog for polysemy-uncontrolled

## v0.1.1.0

* Remove dependency on polysemy-plugin.

## v0.1.0.0

* An insane way to represent evil side effects in polysemy.
